(function() {
  'use strict';

  angular
    .module('sechoAdmin')
    .config(config);

  /** @ngInject */
  function config($logProvider) {
    // Enable log
    $logProvider.debugEnabled(true);
  }

})();
