(function () {
  'use strict';

  angular
    .module('sechoAdmin')
    .controller('LayoutController', LayoutController);

  /** @ngInject */
  function LayoutController($rootScope, moment, $interval, $state) {
    var vm = this;

    $rootScope.$state = $state;

    vm.today = moment().format('YYYY.MM.DD');

    vm.currentTime = moment().format('HH:mm');
    $interval(function () {
      vm.currentTime = moment().format('HH:mm');
    }, 10000);

    $rootScope.tabTimerFlag = true;
    $rootScope.tabTimer = $interval(moveTab, 20000);

    function moveTab() {
      console.log('start timer');
      switch ($state.current.name) {
        case 'main.summary':
          $state.go('main.secho2');
          break;
        case 'main.secho2':
          $state.go('main.secho3');
          break;
        case 'main.secho3':
          $state.go('main.secho4');
          break;
        case 'main.secho4':
          $state.go('main.summary');
          break;
      }
    }

    vm.timeoutBtn = function () {
      console.log('btn');
      if ($rootScope.tabTimerFlag) {
        console.log('stop');
        $interval.cancel($rootScope.tabTimer);
        $rootScope.tabTimerFlag = false;
      } else {
        console.log('start');
        $rootScope.tabTimer = $interval(moveTab, 15000);
        $rootScope.tabTimerFlag = true;
      }
    }

  }
})();
