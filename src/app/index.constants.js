/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('sechoAdmin')
    .constant('c3', c3)
    .constant('moment', moment);
})();
