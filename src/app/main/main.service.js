/**
 * Created by rachel.yang on 2016. 5. 9..
 */
(function () {
  'use strict';

  angular
    .module('sechoAdmin')
    .service('weatherService', weatherService)
    .service('energyService', energyService);


  function weatherService($log, $http, $q) {
    $log.info("START weatherService!!");
    return {
      weather: function () {
        var deferred = $q.defer();

        $http.get('http://61.39.74.111:32770/weather/now')
          .then(function (resp) {

            $log.info('weather/now:: ', resp.data.data);
            var weatherData = resp.data.data;
            switch (weatherData.SKY) {
              case 1:
                weatherData.SKY = 'weather6';
                break;
              case 2:
                weatherData.SKY = 'weather3';
                break;
              case 3:
                weatherData.SKY = 'weather2';
                break;
              case 4:
                weatherData.SKY = 'weather1';
                break;
            }

            deferred.resolve(weatherData);
          });

        return deferred.promise;
      },

      //실내환경
      indoor: function () {
        var deferred = $q.defer();
        //http://61.39.74.111:32770/indoor1/last
        $http.get('http://61.39.74.111:32770/secho/env/last')
          .then(function (resp) {

            $log.info('indoor1/last:: ', resp.data.data);
            var indoor = resp.data.data;
            deferred.resolve(indoor);
          });
        return deferred.promise;
      }
    }

  }


  function energyService($log, $http, $q) {
    $log.info("START energyService!!");

    return {
      //지금 생산량
      lastEnergy: function () {
        var deferred = $q.defer();

        $http.get('http://61.39.74.111:32770/secho/all/last')
          .then(function (resp) {

            $log.info('all/last:: ', resp.data.data);
            var lastEnergyProd = resp.data.data.prod;
            var lastEnergyLoad = resp.data.data.load;
            deferred.resolve({lastEnergyProd: lastEnergyProd, lastEnergyLoad: lastEnergyLoad});
          });
        return deferred.promise;
      },


      //시간별
      timeEnergy: function () {
        var deferred = $q.defer();

        $http.get('http://61.39.74.111:32770/secho/all/day')
          .then(function (resp) {

            $log.info('all/day?calc=sum:: ', resp.data.data);
            var timeEnergyProd = resp.data.data.prod;
            var timeEnergyLoad = resp.data.data.load;
            deferred.resolve({timeEnergyProd: timeEnergyProd, timeEnergyLoad: timeEnergyLoad});
          });
        return deferred.promise;
      },


      //일간 //main 값 포함
      dayEnergy: function () {
        var deferred = $q.defer();

        $http.get('http://61.39.74.111:32770/secho/all/month')
          .then(function (resp) {

            $log.info('all/month?calc=sum:: ', resp.data.data);
            var dayEnergyProd = resp.data.data.prod;
            var dayEnergyLoad = resp.data.data.load;
            deferred.resolve({dayEnergyProd: dayEnergyProd, dayEnergyLoad: dayEnergyLoad});
          });
        return deferred.promise;
      },


      //월간
      monthEnergy: function () {
        var deferred = $q.defer();

        $http.get('http://61.39.74.111:32770/secho/all/year')
          .then(function (resp) {
            $log.info('all/year?calc=sum:: ', resp.data.data);
            var monthEnergyProd = resp.data.data.prod;
            var monthEnergyLoad = resp.data.data.load;
            deferred.resolve({monthEnergyProd: monthEnergyProd, monthEnergyLoad: monthEnergyLoad});
          });
        return deferred.promise;
      },


      //년간
      yearEnergy: function () {
        var deferred = $q.defer();

        $http.get('http://61.39.74.111:32770/secho/all/all')
          .then(function (resp) {
            $log.info('all/all?calc=sum:: ', resp.data.data);
            var yearEnergyProd = resp.data.data.prod;
            var yearEnergyLoad = resp.data.data.load;
            deferred.resolve({yearEnergyProd: yearEnergyProd, yearEnergyLoad: yearEnergyLoad});
          });
        return deferred.promise;
      }

    };
  }


})();
