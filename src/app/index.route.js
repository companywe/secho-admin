(function() {
  'use strict';

  angular
    .module('sechoAdmin')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('main', {
        restrict: 'E',
        // abstract: true,
        templateUrl: 'app/main/layout/layout.html',
        controller: 'LayoutController',
        controllerAs: 'vm'
      })
      .state('main.summary', {
        url: '/',
        templateUrl: 'app/main/summary.html',
        controller: 'SummaryController',
        controllerAs: 'main'
      })
      .state('main.secho2', {
        url: '/secho2',
        templateUrl: 'app/main/secho2.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('main.secho3', {
        url: '/secho3',
        templateUrl: 'app/main/secho3.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('main.secho4', {
        url: '/secho4',
        templateUrl: 'app/main/secho4.html'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
