/**
 * Created by rachel.yang on 2016. 5. 31..
 */
(function () {
  'use strict';

  angular
    .module('sechoAdmin')
    .controller('SummaryController', SummaryController);

  /** @ngInject */
  function SummaryController($log, $state, weatherService, $timeout, energyService, moment) {
    var vm = this;

    $log.info("START summary.controller!!");

    vm.currentState = $state.current.name;
    vm._ = _;

    vm.moveFlag = true; //진행
    vm.timeoutBtn = function () {
      vm.moveFlag = !vm.moveFlag;
    };


    getWeather();
    function getWeather() {
      weatherService.weather().then(
        function (resp) {
          vm.weatherData = resp;
          $timeout(getWeather, 3600000);
        },
        function (reason) {
          vm.weatherData = reason;
        }
      )
    }

    getIndoor();
    function getIndoor() {
      weatherService.indoor().then(
        function (resp) {
          vm.indoor = resp;
          $timeout(getIndoor, 3600000);
        },
        function (reason) {
          vm.indoor = reason;
        }
      )
    }


    getTimeEnergy();
    function getTimeEnergy() {
      energyService.timeEnergy().then(
        function (resp) {
          vm.timeEnergyProd = resp.timeEnergyProd; //boiler, etc, solar
          vm.timeEnergyLoad = resp.timeEnergyLoad; //cooling, heat, ...etc

          vm.timeEnergyLoad5 = _.last(vm.timeEnergyLoad.cooling).value + _.last(vm.timeEnergyLoad.heat).value + _.last(vm.timeEnergyLoad.hotwater).value
            + _.last(vm.timeEnergyLoad.lighting).value + _.last(vm.timeEnergyLoad.venting).value;


          $timeout(getTimeEnergy, 3600000);

        }
      )
    }


    vm.monthX = ['x', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];

    // 노인정 전기사용량, kwh
    vm.year2015elecKwh = [
      {
        'info': '노인정전기 사용기간/사용량 단위 kwh'
      },
      {
        'date': moment('2015-01-01').format('YYYY-MM-DD'),
        'value': 289,
        'cost': 54660
      },
      {
        'date': moment('2015-02-01').format('YYYY-MM-DD'),
        'value': 292,
        'cost': 55210
      },
      {
        'date': moment('2015-03-01').format('YYYY-MM-DD'),
        'value': 264,
        'cost': 50300
      },
      {
        'date': moment('2015-04-01').format('YYYY-MM-DD'),
        'value': 309,
        'cost': 48360
      },
      {
        'date': moment('2015-05-01').format('YYYY-MM-DD'),
        'value': 302,
        'cost': 48810
      },
      {
        'date': moment('2015-06-01').format('YYYY-MM-DD'),
        'value': 314,
        'cost': 51910
      },
      {
        'date': moment('2015-07-01').format('YYYY-MM-DD'),
        'value': 322,
        'cost': 61110
      },
      {
        'date': moment('2015-08-01').format('YYYY-MM-DD'),
        'value': 527,
        'cost': 80590
      },
      {
        'date': moment('2015-09-01').format('YYYY-MM-DD'),
        'value': 459,
        'cost': 69510
      },
      {
        'date': moment('2015-10-01').format('YYYY-MM-DD'),
        'value': 343,
        'cost': 50680
      },
      {
        'date': moment('2015-11-01').format('YYYY-MM-DD'),
        'value': 310,
        'cost': 50700
      },
      {
        'date': moment('2015-12-01').format('YYYY-MM-DD'),
        'value': 137,
        'cost': 41850
      },
      {
        'date': moment('2016-01-01').format('YYYY-MM-DD'),
        'value': 534,
        'cost': 75870
      },
      {
        'date': moment('2016-02-01').format('YYYY-MM-DD'),
        'value': 466,
        'cost': 68910
      },
      {
        'date': moment('2016-03-01').format('YYYY-MM-DD'),
        'value': 274,
        'cost': 52030
      },
      {
        'date': moment('2016-14-01').format('YYYY-MM-DD'),
        'value': 182,
        'cost': 41850
      }
    ];

    //노인정난방 사용기간/사용량, 단위 M3
    vm.year2015gasM3 = [
      {
        'info': '노인정난방 사용기간/사용량 단위 M3'
      },
      {
        'date': moment('2015-01-01').format('YYYY-MM-DD'),
        'value': 222.9331,
        'cost': 223680
      },
      {
        'date': moment('2015-02-01').format('YYYY-MM-DD'),
        'value': 162.9511,
        'cost': 153450
      },
      {
        'date': moment('2015-03-01').format('YYYY-MM-DD'),
        'value': 171.9484,
        'cost': 161230
      },
      {
        'date': moment('2015-04-01').format('YYYY-MM-DD'),
        'value': 164.9611,
        'cost': 137010
      },
      {
        'date': moment('2015-05-01').format('YYYY-MM-DD'),
        'value': 89.9820,
        'cost': 72680
      },
      {
        'date': moment('2015-06-01').format('YYYY-MM-DD'),
        'value': 29.9940,
        'cost': 21160
      },
      {
        'date': moment('2015-07-01').format('YYYY-MM-DD'),
        'value': 2.9994,
        'cost': 2100
      },
      {
        'date': moment('2015-08-01').format('YYYY-MM-DD'),
        'value': 2.9994,
        'cost': 2100
      },
      {
        'date': moment('2015-09-01').format('YYYY-MM-DD'),
        'value': 3.9992,
        'cost': 2820
      },
      {
        'date': moment('2015-10-01').format('YYYY-MM-DD'),
        'value': 15.9968,
        'cost': 2960
      },
      {
        'date': moment('2015-11-01').format('YYYY-MM-DD'),
        'value': 234.9530,
        'cost': 11860
      },
      {
        'date': moment('2015-12-01').format('YYYY-MM-DD'),
        'value': 411.9176,
        'cost': 174680
      }
    ];

    vm.year2015gasKwh = [];
    for (var i = 1; i < vm.year2015gasM3.length; i++) {
      //kwh로 변환 //전기량도 더하는것이 맞는지 확인 + vm.year2015elecKwh[i].value todo
      vm.year2015gasKwh[i] = vm.year2015gasM3[i].value * 9550 * 0.00116222 * 0.1; //임시로 효율 0.1
    }

    vm.lastMonth = moment().subtract(1, 'months').format('M');
    vm.currentMonth = moment().format('M');


    getDayEnergy();
    function getDayEnergy() {
      energyService.dayEnergy().then(
        function (resp) {
          vm.dayEnergyProd = resp.dayEnergyProd; //boiler, etc, solar
          vm.dayEnergyLoad = resp.dayEnergyLoad; //cooling, heat, ...etc

          vm.dayEnergyLoad5 = _.last(vm.dayEnergyLoad.cooling).value + _.last(vm.dayEnergyLoad.heat).value + _.last(vm.dayEnergyLoad.hotwater).value
            + _.last(vm.dayEnergyLoad.lighting).value + _.last(vm.dayEnergyLoad.venting).value;

          $timeout(getDayEnergy, 3600000);

        }
      )
    }


    getMonthEnergy();
    function getMonthEnergy() {
      energyService.monthEnergy().then(
        function (resp) {
          vm.monthEnergyProd = resp.monthEnergyProd; //boiler, etc, solar
          vm.monthEnergyLoad = resp.monthEnergyLoad; //cooling, heat, ...etc


          vm.remodelingAfter = vm.monthEnergyLoad.main[vm.lastMonth - 1].value + vm.monthEnergyLoad.heat[vm.lastMonth - 1].value
            + (vm.monthEnergyLoad.hotwater[vm.lastMonth - 1].value * 1000 * (50 - 15) * 0.00116222 * 0.1); //hotwater ton -> kwh
          vm.remodelingReduce = (vm.year2015gasKwh[vm.lastMonth] + vm.year2015elecKwh[vm.lastMonth].value) - vm.remodelingAfter;
          vm.remodelingReducePercent = vm.year2015gasKwh[vm.lastMonth] / vm.remodelingReduce * 100; //전/(전-후)*100


          vm.monthEnergyLoad5 = _.last(vm.monthEnergyLoad.cooling).value + _.last(vm.monthEnergyLoad.heat).value + _.last(vm.monthEnergyLoad.hotwater).value
            + _.last(vm.monthEnergyLoad.lighting).value + _.last(vm.monthEnergyLoad.venting).value;


          $timeout(getMonthEnergy, 3600000);

        }
      )
    }

  }
})();
